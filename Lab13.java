package day1;

public class Lab13 {
	public static void main(String[] args) {

		int[][] twoD_Array = { { 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9 } };
		for (int[] row : twoD_Array) {
			for (int element : row) {
				System.out.println(element);
				//  ปริ้นค่า ตั้งแต่ 1-9
			}
		}

		int sum = 0;
		for (int row = 0; row < twoD_Array.length; row++) {
			for (int elementg = 0; elementg < twoD_Array[row].length; elementg++) {

				if (elementg == twoD_Array[row].length - 1) {
					sum += twoD_Array[row][elementg];

				}
			}
			System.out.println("Sum of array is " + sum);
			
			// ได้เท่ากับ Sum of array is 3
			// Sum of array is 10
			// Sum of array is 19
		}
	}
}
