package day1;

public class Lab12 {
	public static void main(String[] args) {

		String String1 = "You and Me";
		String String2 = " you and me";

		// ข้อ 1
		if (String1.equals(String2)) {
			System.out.println("มีค่าเท่ากัน");
		} else {
			System.out.println("มีค่าไม่เท่ากัน");
		}
		// ข้อ 2
		String String3 = "and";
		System.out
				.println("ShowString1 = " + String1.contains(String3) + "\nShowString2 = " + String1.contains(String3));

		// ข้อ 3 หาความยาว
		System.out.println("ความยาวของ String1 = " + String1.length() + "\nความยาวของ String2 = " + String2.length());

		// ข้อ 4
		System.out.println(
				"ตัดตำแหน่งที่ 1 ออก = " + String1.substring(1) + "\nตัดตำแหน่งที่ 2 ออก = " + String2.substring(2));

		// ข้อ 5 ตัดช่องว่างในประโยค

		System.out.println("ยังไม่ตัดช่องว่าง =" + String2 + "\nตัดช่องว่าง =" + String2.trim());

		// ข้อ 6 ตัวพิมพ์ใหญ่
		System.out.println("ตัวพิมพ์ใหญ่ String1 = " + String1.toUpperCase() + "\nตัวพิมพ์ใหญ่ String2 = "
				+ String2.toUpperCase());

		// ข้อ 7
		System.out.println("String2 =" + String2.toUpperCase().trim());

	}
}
