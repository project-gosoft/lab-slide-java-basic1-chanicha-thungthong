package day1;

public class Lab5 {
	public static void main(String[] args) {

		int score = 65;
		String grade = null;

		switch (score / 10) {

		case 10:
		case 9:
			grade = "A";
			break;

		case 8:
			grade = "B";
			break;

		case 7:
			grade = "C";
			break;

		case 6:
			grade = "D";
			break;

		case 5:
			grade = "F";
			break;

		default:
			grade = "E";
			break;
		}

		System.out.println("Grade = " + grade);
	}

}