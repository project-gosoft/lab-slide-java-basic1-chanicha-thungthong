package day1;

public class Lab2 {
	public static void main(String[] args) {		
		bark();	
		float a = 2.2f;
		int b = (int)a;
		System.out.println(b);
		
		int c = 5;
		float d = c;
		System.out.println(d);
		
		double e = 6;
		float f = (float)e;
		System.out.println(f);
		
		char q = '1';
		int w = Integer.parseInt(String.valueOf(q));
		System.out.println(w);
		
		final String hello = "Hello";
		//hello = "World"; ถ้าประกาศตัวแปรเป็น final แล้วจะไม่สามารถเปลี่ยนแปลงค่าได้
}
	
	public static void bark() {
		// TODO Auto-generated method stub
		String dogName = "SQL";
		System.out.println("The Dog name = " + dogName + "bark" ) ;
		
	}
	
}
