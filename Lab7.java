package day1;

import java.util.Scanner;

public class Lab7 {
	public static void main(String[] args) {
		
		int counter = 20;
		do {
			System.out.println("Counter: " + counter);
			counter--;
		} while (counter >= 1);
		take();
	}

	public static void take() {
		Scanner sc = new Scanner(System.in);
		int number;
		do {
			System.out.println("กรุณากรอกตัวเลข");
			
			number = sc.nextInt();
			if (number % 2 == 0) {
				System.out.println("เลขคู่");
			} else {
				System.out.println("เลขคี่");
			}
		} while (number % 2 == 0);

	}
}
